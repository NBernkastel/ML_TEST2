import json
import os

with open('Utils/config.json') as json_file:
    config = json.load(json_file)

SECRET_KEY = config["token_key"]


def generate_salt():
    return os.urandom(16).hex()
