from datetime import datetime, timedelta
from hashlib import scrypt
import jwt
from jwt import encode, decode

from Utils.Utils import SECRET_KEY


def verification(password: str, pass_hash: str, salt: str) -> bool:
    if hash_password(password, salt) == pass_hash:
        return True
    return False


def hash_password(password: str, salt: str, hash_length=64) -> str:
    hashed_password = scrypt(
        password.encode('utf-8'),
        salt=salt.encode('utf-8'),
        n=16384,
        r=8,
        p=1,
        dklen=hash_length
    )
    return hashed_password.hex()


def generate_token(login: str, del_time: int = 30) -> str:
    """Time in seconds"""
    expiration = datetime.utcnow() + timedelta(seconds=del_time)
    payload = {
        'login': login,
        'exp': expiration
    }
    token: str = encode(payload, SECRET_KEY, algorithm='HS256')
    return token


def verify_token(token: str) -> jwt.PyJWTError or str:
    try:
        payload = decode(token, SECRET_KEY, algorithms=['HS256'])
        if payload['exp'] < datetime.utcnow().timestamp():
            raise jwt.ExpiredSignatureError("Не валидный токен")

    except (jwt.DecodeError, jwt.ExpiredSignatureError):
        raise jwt.PyJWTError

    return payload['login']
