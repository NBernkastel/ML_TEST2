import jwt
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

from logic import verify_token, generate_token, verification
from models import session, Employee

app = FastAPI()


class LoginData(BaseModel):
    login: str
    password: str


@app.post('/login')
def employee_login(login_data: LoginData):
    employee = session.query(Employee).filter(Employee.login == login_data.login).first()
    if not employee or not verification(login_data.password, employee.password, employee.salt):
        raise HTTPException(status_code=401, detail="Неверный логин или пароль")
    return generate_token(login_data.login)


class TokenData(BaseModel):
    token: str
    inf_type: str


@app.post('/get')
def get_inf(token_data: TokenData):
    try:
        login = verify_token(token_data.token)
    except jwt.PyJWTError:
        raise HTTPException(status_code=401, detail='Неверный токен')
    employee = session.query(Employee).filter(Employee.login == login).first()
    if token_data.inf_type == 'salary':
        return employee.salary
    if token_data.inf_type == 'promotionDate':
        return employee.next_promotion_date
    else:
        raise HTTPException(status_code=404, detail='Неверный адрес')
