FROM python:3.11

RUN pip install poetry

WORKDIR /app

COPY poetry/pyproject.toml poetry/poetry.lock ./

RUN poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-ansi

COPY . .

CMD ["python", "main.py"]
