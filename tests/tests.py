import time
import pytest
from fastapi import HTTPException

from logic import generate_token, verification, verify_token
from controls import employee_login, get_inf
from models import session, Employee


def test_login():
    assert employee_login('Nexeland', 'nexeland') == generate_token('Nexeland')
    with pytest.raises(HTTPException) as exc_info:
        employee_login('Nexeland', 'fghfgh')
        assert str(exc_info) == 'Неверный логин или пароль'
        employee_login('Ne', 'nexeland')
        assert str(exc_info) == 'Неверный логин или пароль'


def test_get_inf():
    token = generate_token('Nexeland', del_time=3)
    res = get_inf(token, 'salary')
    employee = session.query(Employee).filter(Employee.login == 'Nexeland').first()
    assert res == employee.salary
    res = get_inf(token, 'promotionDate')
    assert res == employee.next_promotion_date
    time.sleep(4)
    with pytest.raises(HTTPException) as exc_info:
        get_inf(token, 'salary')
        assert str(exc_info) == 'Неверный токен'


def test_verification():
    password = 'c1683898c6c46e97ea9fea89ca61d478c88f9f631b4dc56fe612febae45c9de9a2' \
               '09cfc1923a32aef55bcb69cb0180527052d863aa0f933ab3461c607590ef85'
    salt = 'd607ae3b049b363ee4bf1b603f19adc0'
    assert verification('hello', password, salt) is True


def test_token():
    login = 'Random'
    token = generate_token(login)
    assert login == verify_token(token)


if __name__ == "__main__":
    pytest.main()
