from sqlalchemy import create_engine, Column, String, Integer, Date
from sqlalchemy.orm import sessionmaker, declarative_base

Base = declarative_base()


class Employee(Base):
    __tablename__ = 'employees'
    login = Column(String(50), unique=True, nullable=False, primary_key=True)
    password = Column(String(128), nullable=False)
    salt = Column(String(32), nullable=False)
    salary = Column(Integer, nullable=False)
    next_promotion_date = Column(Date, nullable=True)


engine = create_engine('sqlite:///mltest.db')
# Base.metadata.create_all(bind=engine)
Session = sessionmaker(bind=engine)
session = Session()
